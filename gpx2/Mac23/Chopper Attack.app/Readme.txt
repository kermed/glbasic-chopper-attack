Chopper Attack v 1.0.17 Beta

--------------
| What is it |
--------------

Chopper Attack is a submittion for Pandora Angst Contest.  Designed from scratch with only 4 days to build.  This is an early beta.  Music and Coding is by me (Lloyd Summers aka KermEd - edware.org).  I can be reached at KermEd@gmail.com.  

Additionally the Aircraft graphics were provided by Prinz Eugn, aka Mark Simpson who can be reached at prinz_eugn@hotmail.com.


--------------
| Whats New  |
--------------

- Fixed Collisions
- Combined GP2X and GP2X Wiz versions
- Updated Awards on Highscore
- Improved Awards
- Added Weapon Power Bar
- Added Weapon Speed Bar
- Changed death reaction
- Changed next-level reaction
- Sped up enemies
- Sped up player
- Repaired rotation issues
- Fixed bug with Power-Up crate adding extra lives
- Added Windows 'cheat' keys

--------
| TODO |
--------

- Code-Cleanup
- Quit-Option from Mid-game
- Finish Missles
- Finish Bosses
- Improve Audio

---------
| Keys: |
---------

GP2X-WIZ
--------
Select       - Pauses
Both Volumes - Exit
Volume       - Sound
Arrows       - Can override the mouse 
B            - Fires
A            - All options in Menu
...          - Missiles [TODO]



Windows
-------
Control      - Pauses
Esc          - Exit
- and =      - Sound
Arrows       - Can override the mouse 
Space        - Fires
...          - Missiles [TODO]

