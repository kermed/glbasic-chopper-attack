--------------
| What is it |
--------------

Chopper Attack is a submittion for a game contest.  Designed from scratch with only 4 days to build.  This is an early beta.  Music and Coding is by me (Lloyd Summers aka KermEd - edware.org).  I can be reached at KermEd@gmail.com.  

Additionally the Aircraft graphics were provided by Prinz Eugn, aka Mark Simpson who can be reached at prinz_eugn@hotmail.com.

---------
| Keys: |
---------

GP2X-WIZ
--------
Select       - Pauses
Both Volumes - Exit
Volume       - Sound
Arrows       - Can override the mouse 
B            - Fires
A            - Missiles [TODO]


Windows
-------
Control      - Pauses
Esc          - Exit
- and =      - Sound
Arrows       - Can override the mouse 
Space        - Fires
...          - Missiles [TODO]

--------
| TODO |
--------

Still on the to-do list is to repair some issues with collision.  
Missles need to be enabled.  
Boss code needs to be finished.  
This needs to be tested on the GP2X.